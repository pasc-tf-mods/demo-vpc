# Default VPC
resource "aws_vpc" "main_vpc" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = var.vpc_name
  }
}

# EIP for NAT Gateway
resource "aws_eip" "nat_eip" {
  vpc = true
}

# Internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main_vpc.id

  tags = {
    Name = var.internet_gateway_name
  }
}

# SUBNETS
resource "aws_subnet" "public_subnets" {
  for_each = var.public_subnets

  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = cidrsubnet(aws_vpc.main_vpc.cidr_block, 8, each.value)
  availability_zone = each.key

  tags = {
    Name = "public_subnet_${each.value}"
  }
}

resource "aws_subnet" "private_subnets" {
  for_each = var.private_subnets

  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = cidrsubnet(aws_vpc.main_vpc.cidr_block, 8, each.value)
  availability_zone = each.key

  tags = {
    Name = "private_subnet_${each.value}"
  }
}

# NAT Gateway
resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public_subnets[keys(var.public_subnets)[0]].id

  tags = {
    Name = var.nat_gateway_name
  }
}

# ROUTE TABLE - towards internet gateway
resource "aws_route_table" "internet_route_table" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "internet-route-table"
  }
}

# ROUTE TABLE - towards NAT Gateway
resource "aws_route_table" "nat_route_table" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block = var.my_ip
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw.id
  }

  tags = {
    Name = "nat-route-table"
  }
}

# ASSOCIATE ROUTE TABLE -- PUBLIC subnet
resource "aws_route_table_association" "internet_route_table_association_app" {
  for_each       = var.public_subnets
  subnet_id      = aws_subnet.public_subnets[each.key].id
  route_table_id = aws_route_table.internet_route_table.id
}

# ASSOCIATE ROUTE TABLE -- PRIVATE subnet
resource "aws_route_table_association" "nat_route_table_association_app" {
  for_each       = var.private_subnets
  subnet_id      = aws_subnet.private_subnets[each.key].id
  route_table_id = aws_route_table.nat_route_table.id
}