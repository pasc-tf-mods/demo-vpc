output "public_subnet_ids" {
  value = [
    for subnet in tolist(aws_subnet.public_subnets.*)[0] :
    subnet.id
  ]
  description = "List of public subnet IDS"
}

output "private_subnet_ids" {
  value = [
    for subnet in tolist(aws_subnet.private_subnets.*)[0] :
    subnet.id
  ]
  description = "List of private subnet IDS"
}

output "vpc_id" {
  value       = aws_vpc.main_vpc.id
  description = "Created VPC ID"
}
