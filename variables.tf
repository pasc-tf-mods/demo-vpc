variable "vpc_cidr" {
  type        = string
  default     = "192.168.0.0/16"
  description = "VPC CIDR"
}

variable "vpc_name" {
  type        = string
  default     = "pasc-tech-demo-vpc"
  description = "Name of the VPC"
}

variable "internet_gateway_name" {
  type        = string
  default     = "pasc-tech-demo-ig"
  description = "Name of the Internet Gateway"
}

variable "public_subnets" {
  type = map
  default = {
    eu-west-1a = "1"
    eu-west-1b = "2"
  }
  description = "Map of AZ for the public subnet"
}

variable "private_subnets" {
  type = map
  default = {
    eu-west-1c = "3"
  }
  description = "Map of AZ for the private subnet"
}

variable "nat_gateway_name" {
  type        = string
  default     = "NAT-GW"
  description = "Name of the NAT Gateway"
}

variable "my_ip" {
  type        = string
  description = "IP of control machine"
}
