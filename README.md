## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| aws | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| internet\_gateway\_name | Name of the Internet Gateway | `string` | `"pasc-tech-demo-ig"` | no |
| my\_ip | IP of control machine | `string` | n/a | yes |
| nat\_gateway\_name | Name of the NAT Gateway | `string` | `"NAT-GW"` | no |
| private\_subnets | Map of AZ for the private subnet | `map` | <pre>{<br>  "eu-west-1c": "3"<br>}</pre> | no |
| public\_subnets | Map of AZ for the public subnet | `map` | <pre>{<br>  "eu-west-1a": "1",<br>  "eu-west-1b": "2"<br>}</pre> | no |
| vpc\_cidr | VPC CIDR | `string` | `"192.168.0.0/16"` | no |
| vpc\_name | Name of the VPC | `string` | `"pasc-tech-demo-vpc"` | no |

## Outputs

| Name | Description |
|------|-------------|
| private\_subnet\_ids | List of private subnet IDS |
| public\_subnet\_ids | List of public subnet IDS |
| vpc\_id | Created VPC ID |

